<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class CommentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = \App\Models\User::all();
        $articles = \App\Models\Article::all();
        for($i = 0; $i < 20; $i++) {
            \App\Models\Comment::factory()
                ->for($users->random())
                ->for($articles->random())
                ->create();
        }
    }
}
