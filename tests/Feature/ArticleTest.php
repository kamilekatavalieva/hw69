<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Laravel\Passport\Passport;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class ArticleTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_success_get_all_articles()
    {
        $user = \App\Models\User::factory()->create();
        Passport::actingAs($user, [route('articles.index')]);
        $articles = \App\Models\Article::factory()->for($user)->count(3)->create();
        $response = $this->getJson(route('articles.index'));
        $response->assertOk();
        $response->assertJsonStructure(
            [
                'data',
                'meta' => [
                    'current_page',
                    'from',
                    'last_page',
                    'links',
                    'path',
                    'per_page',
                    'to',
                    'total',
                ],
                'links',
            ]
        );
        $this->assertCount($articles->count(), $response->json()['data']);
    }

    public function test_not_aut_user_trying_get_all_articles()
    {
        $user = \App\Models\User::factory()->create();
        \App\Models\Article::factory()->for($user)->count(3)->create();
        $response = $this->getJson(route('articles.index'));
        $response->assertUnauthorized();
    }

    public function test_user_success_get_once_article()
    {
        $user = \App\Models\User::factory()->create();
        $article = \App\Models\Article::factory()->for($user)->create();
        Passport::actingAs($user, [route('articles.show', compact('article'))]);
        $response = $this->getJson(route('articles.show', compact('article')));
        $response->assertOk();
        $response->assertJsonStructure(
            [
               'data' => [
                   'id',
                   'title',
                   'content',
                   'user' => [
                       'id',
                       'email'
                   ]
               ]
            ]
        );
        $this->assertEquals($article->id, $response->json()['data']['id']);
        $this->assertEquals($article->title, $response->json()['data']['title']);
        $this->assertEquals($article->content, $response->json()['data']['content']);
        $this->assertEquals($article->user_id, $response->json()['data']['user']['id']);
    }

    public function test_not_auth_user_trying_get_once_article()
    {
        $user = \App\Models\User::factory()->create();
        $article = \App\Models\Article::factory()->for($user)->create();
        $response = $this->getJson(route('articles.show', compact('article')));
        $response->assertUnauthorized();
    }

    /**
     * A basic feature test example.
     * @group delete_article
     * @return void
     */
    public function test_user_success_delete_article()
    {
        $user = \App\Models\User::factory()->create();
        $article = \App\Models\Article::factory()->for($user)->create();
        Passport::actingAs($user, [route('articles.destroy', compact('article'))]);
        $this->assertDatabaseHas('articles', [
            'title' => $article->title,
            'content' => $article->content,
            'user_id' => (string) $article->user_id,
            'id' => (string) $article->id
        ]);
        $response = $this->deleteJson(route('articles.destroy', compact('article')));
        $response->assertNoContent();
        $this->assertDatabaseMissing('articles', [
            'title' => $article->title,
            'content' => $article->content,
            'user_id' => (string) $article->user_id,
            'id' => (string) $article->id
        ]);
    }

    /**
     * A basic feature test example.
     * @group delete_article
     * @return void
     */
    public function test_user_can_not_delete_not_his_article()
    {
        $user = \App\Models\User::factory()->create();
        $another_user = \App\Models\User::factory()->create();
        $article = \App\Models\Article::factory()->for($user)->create();
        Passport::actingAs($another_user, [route('articles.destroy', compact('article'))]);
        $this->assertDatabaseHas('articles', [
            'title' => $article->title,
            'content' => $article->content,
            'user_id' => (string) $article->user_id,
            'id' => (string) $article->id
        ]);
        $response = $this->deleteJson(route('articles.destroy', compact('article')));
        $response->assertForbidden();
        $this->assertDatabaseHas('articles', [
            'title' => $article->title,
            'content' => $article->content,
            'user_id' => (string) $article->user_id,
            'id' => (string) $article->id
        ]);
    }

    /**
     * A basic feature test example.
     * @group delete_article
     * @return void
     */
    public function test_not_auth_user_can_not_delete_article()
    {
        $user = \App\Models\User::factory()->create();
        $article = \App\Models\Article::factory()->for($user)->create();
        $this->assertDatabaseHas('articles', [
            'title' => $article->title,
            'content' => $article->content,
            'user_id' => (string) $article->user_id,
            'id' => (string) $article->id
        ]);
        $response = $this->deleteJson(route('articles.destroy', compact('article')));
        $response->assertUnauthorized();
        $this->assertDatabaseHas('articles', [
            'title' => $article->title,
            'content' => $article->content,
            'user_id' => (string) $article->user_id,
            'id' => (string) $article->id
        ]);
    }


}
