<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\ArticleStoreRequest;
use App\Http\Resources\ArticleResource;
use App\Models\Article;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Http\Response;

class ArticleController extends Controller
{
    /**
     * ArticlesController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth:api')->except(['index']);
    }

    /**
     * @return AnonymousResourceCollection
     */
    public function index():AnonymousResourceCollection
    {
        $articles = Article::paginate(5);
        return ArticleResource::collection($articles);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ArticleStoreRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(ArticleStoreRequest $request)
    {
        $data = $request->validated();
        $data['user_id'] = auth('api')->user()->getAuthIdentifier();
        $article = Article::create($data);
        return response()->json(new ArticleResource($article), 201);
    }


    /**
     * @param Article $article
     * @return ArticleResource
     */
    public function show(Article $article):ArticleResource
    {
        return new ArticleResource($article);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ArticleStoreRequest $request
     * @param Article $article
     * @return ArticleResource
     */
    public function update(ArticleStoreRequest $request, Article $article):ArticleResource
    {
        $article->update($request->validated());
        $article->fresh();
        return new ArticleResource($article);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Article $article
     * @return Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function destroy(Article $article):Response
    {
        $this->authorize('delete', $article);
        $article->delete();
        return response([], 204);
    }
}
