<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\CommentRequest;
use App\Http\Resources\CommentResource;
use App\Models\Comment;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Http\Response;

class CommentController extends Controller
{

    /**
     * CommentsController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth:api')->except(['index']);
    }


    /**
     * Display a listing of the resource.
     *
     * @return AnonymousResourceCollection
     */
    public function index():AnonymousResourceCollection
    {
        $comments = Comment::all();
        return CommentResource::collection($comments);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param CommentRequest $request
     * @return CommentResource
     */
    public function store(CommentRequest $request)
    {
        $comment = Comment::create($request->all());
        return new CommentResource($comment);
    }


    /**
     * @param Comment $comment
     * @return CommentResource
     */
    public function show(Comment $comment):CommentResource
    {
        return  new CommentResource($comment);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param CommentRequest $request
     * @param Comment $comment
     * @return CommentResource
     */
    public function update(CommentRequest $request, Comment $comment):CommentResource
    {
        $comment->update($request->validated());
        $comment->fresh();
        return new CommentResource($comment);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Comment $comment
     * @return Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function destroy(Comment $comment):Response
    {
        $this->authorize('delete', $comment);
        $comment->delete();
        return response([], 204);
    }
}
